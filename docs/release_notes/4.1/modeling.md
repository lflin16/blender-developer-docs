# Blender 4.1: Modeling

## Meshes

### Auto Smooth

The "Auto Smooth" option has been replaced by a modifier node group asset
(blender/blender@89e3ba4e25c9ff921b2584c294cbc38c3d344c34).

- The base state of meshes is now the same as having "Auto Smooth" on
  with an angle of 180 degrees in older versions:
  - Face corner "split" normals are calculated when there is a mix of
    sharp and smooth elements.
  - Custom normals are used if available.
- The modifier node group asset sets sharpness by the angle between
  faces.
- Blender automatically chooses whether to calculate vertex, face, or
  face corner normals given the presence of custom normals and the mix
  of sharp/smooth edges and faces.
- In edit mode, the new **Set Sharpness by Angle** operator can be used to
  set sharpness, or "Shade Smooth by Angle" in other modes
  (blender/blender@78963b466b1fabc0f827a2e298957b47ec2a971f).
- For more information on the procedural smoothing modifier, see the
  [Nodes & Physics](nodes_physics.md)
  section.
- For more information on the Python API changes, see the
  [Python API](python_api.md#breaking-changes) section.

### Other

- A render "simplify" setting adds the ability to turn off calculation
  of face corner and custom normals in the viewport
  (blender/blender@912c4c60d8b3bdf741d9cdcb6d04d17029b02b69).
- Shape Keys can now be locked to protect them from accidental edits via
  sculpting or edit mode transformation tools and operators.
  (blender/blender@b350d7a4c311ec6a3065ac89febafc3ba6bd43a2,
  [Manual](https://docs.blender.org/manual/en/4.1/animation/shape_keys/shape_keys_panel.html#bpy-types-shapekey-lock-shape))
- Face corner normals are cached and are recalculated in fewer cases
  (blender/blender@62dbef895bb854df8a1a14db033c367be4a8a7f2).
- The creation of mesh topology maps used in geometry nodes and normals calculation
  is parallelized and elsewhere and can be over 5 times faster
  (blender/blender@bb80716244d3539d52439585b68f48d734197b55).

## Curves

The new curves type supports new operators.

- The "Draw" tool (blender/blender@feae0b417395873d72761b5fbe78721b68f12fea).
  - The draw tool has an option to create NURBS curves directly
    (blender/blender@70fe812ef1ac98c681772b3e92bc288dcc88f128)
- The "Extrude" operator (blender/blender@0d964f91a289de2fb4af2d59ee2957147d74c93d).
- The "Duplicate" operator (blender/blender@9af176bfe661cea4e46c9e3eec2628adf499dd00).
- Operators to control tilt (blender/blender@0e8c8741665bf1d4e6a88b3614a3f5185ab34a31).
