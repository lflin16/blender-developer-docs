# Blender 4.2: Sculpt, Paint, Texture

## UI
*Sculpt* and *Weight Paint* now use the global, customizable rotation increment for corresponding Line tools when snapping is enabled (e.g. *Line Hide* in *Sculpt* and *Gradient* in *Weight Paint*). (blender/blender@1cf0d7ca6a858b5c7a0b03a224d9e826ffa38c47)

## Sculpting
- Add *Polyline Mask* tool.
  (blender/blender@025df21a0f7e21e164fe82ab4e22159836ea6675)

- Add *Lasso Hide* tool.
  (blender/blender@68afd225018e59d63431e02def51575bfc76f5cb)

<figure>
<video src="../../../videos/4.2_sculpt_lasso_hide.mp4"
       title="Demo of Lasso Hide tool" width="400" controls=""></video>
<figcaption>Demo of Lasso Hide tool</figcaption>
</figure>

- Add *Line Hide* tool.
  (blender/blender@6e997cc75723b0dfc4d25d8246f373d46a662905)

<figure>
<video src="../../../videos/4.2_sculpt_line_hide.mp4"
       title="Demo of Line Hide tool" width="400" controls=""></video>
<figcaption>Demo of Line Hide tool</figcaption>
</figure>

- Add *Polyline Hide* tool.
  (blender/blender@55fc1066acf69dd04f7f7b1c3af4c3f360769523)

<figure>
<video src="../../../videos/4.2_sculpt_polyline_hide.mp4"
       title="Demo of Polyline Hide tool" width="400" controls=""></video>
<figcaption>Demo of Polyline Hide tool</figcaption>
</figure>

- Add *Line Face Set* tool.
  (blender/blender@7726e7f563190620034f43a19aa01b9907a26530)

- Add *Polyline Face Set* tool.
  (blender/blender@025df21a0f7e21e164fe82ab4e22159836ea6675)

- Add option to choose between *Fast* and *Exact* solvers for *Trim* tools.
  (blender/blender@881178895b8528fd37ef41cd7a4bc481f7b7d311, [Manual](https://docs.blender.org/manual/en/4.2/sculpt_paint/sculpting/tools/box_trim.html#tool-settings))

- Add *Line Trim* tool.
  (blender/blender@d4a61647bf571cb90090e1320f4aa1156482836f)

- Add *Polyline Trim* tool.
  (blender/blender@025df21a0f7e21e164fe82ab4e22159836ea6675)
