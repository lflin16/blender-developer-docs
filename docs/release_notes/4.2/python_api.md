# Blender 4.2: Python API & Text Editor

## Add-on Packaging

With the [new extensions platform](extensions.md), there is also a new method for packaging add-ons. It uses a separate `blender_manifest.toml` file instead of `bl_info` embedded in the script. See the [extensions user manual](https://docs.blender.org/manual/en/4.2/extensions/getting_started.html) for details.

The old packaging still works but is considered legacy, and add-ons are recommended to switch to the new manifest file.

## Text Editor

- Support for GLSL syntax highlighting
  (blender/blender@462c144f414343ffbbac3546f1fae2bbf0bd52db)

## Additions

### `bpy.app.online_access`

- Read-only property to check whether Blender (and add-ons) is expected to access the internet.
- Add-ons are expected to check for this property because doing any access to the internet.
- Also available is `bpy.app.online_access_override` which tells whether Blender was launched with `--offline-mode` / `--online-mode`.

See https://docs.blender.org/manual/en/dev/advanced/extensions/addons.html#internet-access

### `bpy.utils` Module

- The `EXTENSIONS` option has been removed from `bpy.utils.user_resource()`
  (blender/blender@6bfc8612bf757f24fe1abaa03dbf4bc194cfae4d).
- Add `bpy.utils.register_cli_command` & `unregister_cli_command`
  so scripts can define their own command line functionality via (`-c`, `--command`).
  (blender/blender@9372e0dfe092e45cc17b36140d0a3182d9747833)

### Animation

- `rna_struct.keyframe_insert()` now has a new, optional keyword argument `keytype` to set the key
  type (blender/blender@1315991ecbd179bbff8fd08469dda61ec830d72f).
  This makes it possible to set the new key's type directly.
  Example: `bpy.context.object.keyframe_insert("location", keytype='JITTER')`.

### Layout Panels

Added support for layout panels for operator properties drawing, in popups, redo panel and the file browser sidebar. Add-ons are recommended to use panels for organizing properties now, to follow Blender user interface conventions. (blender/blender@aa03646a74, blender/blender@02681bd44d285b51ffe9cf6d3df71af1980a6f70)

### Exporters

Exporter add-ons can add support for the new [Collection Exporters](pipeline_assets_io.md#collection-exporters). This requires a [`FileHandler`](https://docs.blender.org/api/main/bpy.types.FileHandler.html) with `bl_export_operator`, and a string property named `collection` on the operator to indicate the collection to export. See the FBX and glTF add-ons for an example.

### Modal Operators

- `Window.modal_operators` provides a list of running modal operators. (blender/blender#122193)
- Modal operator can add `MODAL_PRIORITY` to `bl_options` to handle events before other modal operators. (blender/blender@6efd7a6a507c2691d2de72ced9ea9577a68a4f3b)

### Handlers
- Render: include info string for "render_stats" handler (blender/blender@fcc481a40746014465bc2a6c025abd5ee4b0e3fa)

## Breaking changes

### `bpy.utils` Module

- The `AUTOSAVE` option has been removed from `bpy.utils.user_resource()`
  (blender/blender@6bfc8612bf757f24fe1abaa03dbf4bc194cfae4d).

### Render Settings

- Motion Blur settings have been de-duplicated between Cycles and EEVEE and moved to
  `bpy.types.RenderSettings`. (blender/blender@74b8f99b43)
  - `scene.cycles.motion_blur_position` -> `scene.render.motion_blur_position`
  - `scene.eevee.use_motion_blur` -> `scene.render.user_motion_blur`
  - `scene.eevee.motion_blur_position` -> `scene.render.motion_blur_position`
  - `scene.eevee.motion_blur_shutter` -> `scene.render.motion_blur_shutter`
- Light cast shadow setting have been de-duplicated between Cycles and EEVEE and moved to
  `bpy.types.Light`. (blender/blender@1036d9bdb2) 
  - `light.cycles.cast_shadow` -> `light.use_shadow`

### Nodes

- Unused `parent` argument removed from the `NodeTreeInterface.new_panel` function
  for creating node group panels. (blender/blender#118792)
- Some node properties have been renamed to fix name collisions
  (blender/blender@deb332601c2a5c5c41df21543c08ac1381ca4a0a):
  - Compositor Box/Ellipse Mask node: `width` -> `mask_width` (same for `height`)
  - Shader AOV Output node: `name` -> `aov_name`
  - Geometry Color node: `color` -> `value`

### Image Object Operators

- The "add image object" operator has been deduplicated and unified into one operator:
  `object.empty_image_add`. (blender/blender@013cd3d1ba)<br>
  Previously there were two operators to add image empties
  (`object.load_reference_image` and `object.load_background_image`)
  with a separate operator for dropping (`object.drop_named_image`).
- The "add/remove background image from camera" operator has been renamed to clarify that this is
  only for the camera object (blender/blender@013cd3d1ba):
  - `view3d.background_image_add` -> `view3d.camera_background_image_add`.
  - `view3d.background_image_remove` -> `view3d.camera_background_image_remove`.