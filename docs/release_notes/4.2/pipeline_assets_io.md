# Blender 4.2: Pipeline, Assets & I/O

## Collection Exporters
File format exporters can now be associated with Collections. One or
more exporters can be added and configured in the Collection properties
panel. (blender/blender@509a7870c3570cbf8496bcee0c94cdc1e9e41df5)

This feature streamlines the process of re-exporting the same asset(s)
repeatedly. For example when creating glTF assets for a game and
iterating on the look, or when using Blender in a studio pipeline to
create USD assets.

All collections in the scene can be exported with a single
click through the File menu. Additionally, each collection can be
exported to multiple file formats simultaneously.

It also provides a central place to consolidate export settings which
are stored inside the .blend file for easy sharing and persistence
across Blender sessions.

![](./images/io_collection_export.png){style="width:400px;"}

## USD
- The new Hair `Curves` object type is now supported for both Import and Export (blender/blender@ea256346a89, blender/blender@21db0daa4e5)
- Point Cloud import is now supported. (blender/blender@e4ef0f6ff4fe1df8188a85e2a6b33e62801085e3)
- New options for Import
  - Added option to import only defined prims or all (blender/blender@bfa54f22ceb)
  - Convert USD Dome Light to a World shader (blender/blender@e1a6749b3d9)
  - Option to validate meshes on import (blender/blender@2548132e23f)
- New options for Export
  - Filtering which types to export (blender/blender@a6a5fd053a3cd036ac06028dc11c5b4c37e7935a)
  - Converting the World shader to a USD Dome Light (blender/blender@e1a6749b3d9)
  - Selection for Stage Up axis (blender/blender@24153800618)
  - Selection for XForm operator convention (blender/blender@36f1a4f94f3)
  - Triangulate meshes (blender/blender@b2d1979882f)
  - Down-sample exported textures when exporting to USDz (blender/blender@3e73b9caa5a)
 - Generate MaterialX network from Blender shader nodes on export. This supports a subset of all shader nodes and their functionality, and is currently disabled by default. (blender/blender!122575)

<!--
- Other notable improvements ...
-->

## Alembic
- The new Hair `Curves` object type is now supported for both Import and Export (blender/blender@ea256346a89, blender/blender@b24ac18130e)
- Addressed long-standing issue where animated curves would not update during render (blender/blender@ea256346a89)

## Collada
The built-in support for this format is now [considered as legacy](https://devtalk.blender.org/t/moving-collada-i-o-to-legacy-status/34621) (blender/blender@82f9501a4a, blender/blender-manual@20e4984ad6).

## General
- OBJ, PLY, STL importers now default to "Validate Meshes" option being on (blender/blender@761dd6c9238).
  This makes import process a bit slower, but prevents later crashes in case input files
  contain malformed data. Validation itself is now 2-3x faster compared to previous versions (blender/blender!121413).