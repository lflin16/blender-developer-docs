# Blender 4.2: Core

## Portable Installation

- Creating a portable installation now requires creating a folder named `portable` next to the Blender executable, instead of a `4.2/config` directory. (blender/blender!122778)

## Environment Variables

- The `BLENDER_SYSTEM_SCRIPTS` environment variable can now be used to add scripts likes add-ons and presets to a Blender installation. This can be used for example in an animation studio to make scripts available to all users. Previously this environment variable often either did not work or behaved inconsistently. (blender/blender!122689)

## Command Line Arguments

- Running in background-mode using `-b` or `--background` now disables audio by default
  (blender/blender@7c90018f2300646dbdec2481b896999fe93e6e62).
- New `--online-mode` and `--offline-mode` arguments to force Blender to enable or disable online access, overriding the preferences. (blender/blender!121994)

## Undo

- Faster undo due to implicit sharing (blender/blender@0e8e219d71cd27cf025a9920ac4fb54ff7c178b3).