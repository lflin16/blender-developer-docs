# Grease Pencil

## Operators

- Improved smooth algorithm.
  (blender/blender@d4e1458db3a0)

## UI

- Scale Stroke Thickness is now visible in Pie menu.
  (blender/blender@acd7a648b17a)

![Pie Menu](../../images/Pie_Menu_1.png){style="width:800px;"}

## Modifiers and VFX

- New modifier: Envelope
  (blender/blender@cee6af00567f)

![](../../images/Envelope_Modifier.png){style="width:800px;"}

- Dot Dash modifier:
  - Lowered the bounds limits for dash and gap
    (blender/blender@8d4244691a2d)

![](../../images/Dot_dash.png){style="width:800px;"}

- - New option to use Cyclic per segment
    (blender/blender@97f2210157db)

![Dot dash Cyclic](../../images/Dot_dash_04.png){style="width:800px;"}

- Smooth Modifier: New option to keep the overall shape closer to the
  original, avoiding the strokes to shrink
  (blender/blender@d4e1458db3a0)

![](../../images/Smooth_Modifier.png){style="width:800px;"}

- Build modifier:
  - New Additive mode.
    (blender/blender@e74838d0d011)
  - Adds fade support
    (blender/blender@c4e4924096d1)
  - Adds origin object for the building process
    (blender/blender@c4e4924096d1)

![Build Modifier](../../images/Build_Modifier.png){style="width:800px;"}

## Performance

- Added a cache to speed up Grease Pencil object evaluation. This is
  currently used only for drawing strokes and sculpting using some
  brushes like the push brush.
  (blender/blender@e2befa425a84)
- Improve multi-user Grease Pencil data performance with modifiers.
  (blender/blender@7ca13eef7c33)

## Bug Fix

- Grease Pencil edit mode overlays now support the clipping region.
  (blender/blender@69a43069e857)
