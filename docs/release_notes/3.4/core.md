# Core

## Overriding Resource Directories

It is now possible to override all `USER` and `SYSTEM` paths using
environment variables:

- `BLENDER_USER_RESOURCES` (accessed via
  `bpy.utils.resource_path('USER')`).
- `BLENDER_SYSTEM_RESOURCES` (accessed via
  `bpy.utils.resource_path('SYSTEM')`).

Previously, sub-directories could be overridden via environment
variables such as `BLENDER_USER_CONFIG` and `BLENDER_SYSTEM_PYTHON`
(see the `--help` message for a full list). However there was no way
to override the top level directories which scripts could access via
`bpy.utils.resource_path`. Now the top-level directories can be
overridden so the default USER or SYSTEM resource paths aren't used by
accident
(blender/blender@bf4926b30c6fc7b9f98dde508b7b644feaf21022).

## Fonts

- New stack of fonts for improved language and symbol coverage
  (blender/blender@e9bd6abde37c).
- FreeType caching to allow more simultaneous fonts with less resources
  and increased performance.
  (blender/blender@d39abb74a0a9)
- Avoid loading fonts that are not actually used
  (blender/blender@c0845abd897f).
- Font sizing now always assumes 72 DPI, simplifying code. DPI argument
  to blf.size() is now optional and deprecated.
  (blender/blender@cd1631b17dd0)
- Gamma correction for text output for improved anti-aliasing. Result is
  also *slightly* fuller and brighter.
  (blender/blender@d772e11b5a1e)

## Performance

- Create preview thumbnails of WebP images a little quicker while using
  much less RAM.
  (blender/blender@8851790dd733)
- View layer sync operations are now postponed until the sync results
  are needed. This greatly speeds up scripts that create many objects in
  a single operation
  (blender/blender@68589a31eb).
- Depsgraph: Optimize evaluation of dependencies of disabled modifiers
  (blender/blender@f12f7800c29)

## Others

- Metaball objects are now evaluated as meshes. Render engines now only
  need to process the corresponding evaluated mesh object and can skip
  the evaluated metaball object.
  (blender/blender@eaa87101cd5a).
- Support extracting frames from WebM videos that dynamically change
  resolution.
  (blender/blender@d5554cdc7c90).
- Video rendering: Support FFmpeg AV1 codec encoding.
  (blender/blender@59a0b49c100b8444a15f4713409004eade9fd321).
- libOverride: RNA API: Add option to make all overrides editable in
  `override_hierarchy_create`
  (blender/blender@a67b33acd04ea48).
- Support for the Wayland graphics system is now enabled for Linux
  (blender/blender@f9ab2214ae52c51c068ceff97e5264fd518d8f59).
