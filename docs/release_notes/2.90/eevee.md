# Blender 2.90: EEVEE

## Motion Blur Improvement

The motion blur has been done from scratch and now support mesh
deformation, hair, and sub-frame accumulation for even better precision.
For more details [check out the
manual](https://docs.blender.org/manual/en/dev/render/eevee/render_settings/motion_blur.html#example).
(blender/blender@f84414d6e14c
blender/blender@439b40e601f8)

![](../../images/Mb_32step_fx.png){style="width:600px;"}

## Improvements

- Fix texture node repeat & filter mode to match Cycles better.
  (blender/blender@b2dcff4c21a6)

## Sky Texture

- Port of Preetham and Hosek/Wilkie sky models.
  (blender/blender@9de09220fc5f)

![](../../images/Hosek_Eevee.png){style="width:600px;"}
