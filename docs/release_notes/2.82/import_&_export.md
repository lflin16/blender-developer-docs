# Blender 2.82: Import & Export

## Universal Scene Description

USD file export is now supported, using File → Export → Universal Scene
Description. This is an initial implementation, and USD support will be
improved over time to support more complete scene interchange.
Information can be found in the [USD chapter in the Blender
manual](https://docs.blender.org/manual/en/dev/files/import_export/usd.html).

![USD file exported from Blender and shown in USDView](../../images/Blender-2.82-USD-usdview-spring.png){style="width:600px;"}

For developers, there is more [USD technical
documentation](../../features/objects/io/usd.md) about the
implementation.
(blender/blender@ec62413f803ee506)

## Alembic

- Mesh normals are now exported properly to Alembic.
  ([Manual](https://docs.blender.org/manual/en/dev/files/import_export/alembic.html#custom-split-normals-of-meshes)).
  (blender/blender@b0e7a1d4b492)
- Changed "Visible Layers" to "Visible Objects" in export options.
  Similarly, the Alembic exporter operator
  `bpy.ops.wm.alembic_export()` changed the keyword argument
  `visible_layers_only` to `visible_objects_only`.
  (blender/blender@d9e61ce1953b)

## glTF

### Importer

- Multiple files import feature
  (blender/blender-addons@67e42c79e5c3)
- Better names for UVMap and VertexColor
  (blender/blender-addons@08352b3af770)
- Size of bone at creation are now based on armature scale
  (blender/blender-addons@76fc4142b518)
- Import data from texture samplers + exporter fix
  (blender/blender-addons@9f1b91ca1316)
- Use vertexcolor node instead of attributenode
  (blender/blender-addons@74a8fb2aa8f9)
- Set custom props from extras for bones
  (blender/blender-addons@0141c7f4cf49)
- Accessor/buffer decoding enhancements
  (blender/blender-addons@eb4085f1dc15,
  blender/blender-addons@47bef1a8a535)
- Import extra data as custom properties
  (blender/blender-addons@ecdaef952383)
- Fix bad vertex color alpha import
  (blender/blender-addons@6087f4c26540)
- Fix avoid crash on invalid glTF file with multiple same target path
  (blender/blender-addons@9189c4595844)
- Fix import of bone rest pose of negative scaled bones
  (blender/blender-addons@2b04a1ab6829)
- Fix bug assigning pose bone custom prop
  (blender/blender-addons@fe5f339eb0b4)

### Exporter

- Option to export only deformation bones
  (blender/blender-addons@b9b1814a4c26)
- Basic SK driver export (driven by armature animation)
  (blender/blender-addons@f505743b2f9f)
- Performance improvement on image export
  (blender/blender-addons@289fb2b8b89e)
- Define a user extension API
  (blender/blender-addons@18246268e802,
  blender/blender-addons@73b85949a06c,
  blender/blender-addons@9733d0f73654,
  blender/blender-addons@8304b9bac7dc)
- Alphabetically sorting of exported animation
  (blender/blender-addons@5f1ad50707e7)
- Add option to export textures into a folder
  (blender/blender-addons@60a11a0fc442)
- Keep constant/step interpolation when sampled animation export
  (blender/blender-addons@b90cbbdf4801)
- Detach last exported animation if needed
  (blender/blender-addons@bad59573c39e)
- Add export support for Emission socket of Principled BSDF node
  (blender/blender-addons@78fe8ec049fa)
- Do not split in primitives if materials are not exported
  (blender/blender-addons@d709b46340b5)
- Remove primitive splitting
  (blender/blender-addons@45c87c1ae354)
- Export action custom props as animation extras
  (blender/blender-addons@334ca375b5e6)
- Fix exporting area lights that are not supported
  (blender/blender-addons@c172be1c93a6)
- Fix exporting instances when apply modifier is disabled
  (blender/blender-addons@c23d2a741e21)
- Fix crash when armature action is linked to mesh object
  (blender/blender-addons@7003720257c9,
  blender/blender-addons@a6ce1b121eb7)
- Fix transforms for mesh parented directly to bones
  (blender/blender-addons@a96fa1e6111a)
- Fix skinning export when using draco compression
  (blender/blender-addons@1470f353c650)
- Fix extension required parameter check
  (blender/blender-addons@619f3b5b2d8c)
- Fix animation that was removed when using both TRS + SK animation
  (blender/blender-addons@24cc252dc6f6)
- Fix Draco bug when exporting Blender instances
  (blender/blender-addons@d504a09ab5ea)
- Fix buffer size with Draco compression
  (blender/blender-addons@dc0281955383)
- Allow draco compression when exporting without normals
  (blender/blender-addons@5367ebad4813)
- Fix export of light with shader node
  (blender/blender-addons@329f2c4a8b9f)
- Fix normals when changed by modifiers
  (blender/blender-addons@6238248739dc,
  blender/blender-addons@3a774beff73d)
- Fix avoid exporting rotation twice when both euler and quaternion
  (blender/blender-addons@97b11857d320)

## VFX Reference Platform

Blender is now compatible with the [VFX Reference
Platform](https://vfxplatform.com) 2020 for library dependencies and
Python. This helps improve compatibility between Blender and other VFX
software, especially now that the VFX industry is also switching to
Python 3.
