## Bevel

### Custom Profiles

The bevel tool and modifier now have the ability use modified profiles
beyond the existing super-ellipse control
(blender/blender@ba1e9ae4733a).

Once the "Custom Profile" option is checked, the profile can be changed
with the curve widget which is sampled with the number of segments from
the bevel.

The orientation of asymmetrical profiles is regularized, so that the
profile will be flipped consistently on successive groups of edges.

Presets such as steps and a few simple architectural mouldings are
included.

![A simple custom profile along a curving group of edges](../../images/Custom_Bevel_Profile_Release_Notes.png){style="width:600px;"}

### Cutoff Vertex Mesh Method

A simpler method of creating meshes to replace vertices was added,
mostly for use with more complicated asymmetrical bevel profiles where
greater than two-way intersections can result in arbitrary geometry
(blender/blender@ba1e9ae4733a).

![An example of the cutoff method](../../images/GSoC2019_Bevel_Cutoff_Method.png){style="width:400px;"}

## Weld Modifier

Now we have the weld modifier that merges groups of vertices within a
threshold
(blender/blender@f050fa325375).
![Weld modifier panel](../../images/Weld_Modifier_Panel.png){style="width:600px;"}

## Solidify Modifier

The solidify modifier is now able to create thickness in edges with more
two adjacent faces, for example when three walls of a building join in a
T-formation. It is now also able to produce perfect results on meshes
with a normal flip seam like a mobius strip. The algorithm to calculate
the positioning of the vertices is also improved to get much closer to
perfectly even thickness than it was possible with the old simple
extrude algorithm. Also the new algorithm will handle degenerate cases
as well as possible.
(blender/blender@e45cfb574ee7).

There is now an option to use the angle for clamping as well as the
length of the shortest adjacent edge from before. This will give better
results in many cases.
(blender/blender@e45cfb574ee7).

![Improved Solidify Modifier](../../images/Blender_2-82_releasenotes_updatesolidify2.png){style="width:600px;"}
