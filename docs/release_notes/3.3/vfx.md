# VFX & Video

## Tracking

Implemented "Image from Plane Marker" operator which creates or updates
image data-block from pixels which the plane marker "sees"
(blender/blender@e4bf58e2852).

This allows creating unwarped texture from a billboard from footage. The
intent is to allow this image to be touched up and re-projected back to
the footage with an updated content.

<youtube>PDphO-w2SsA</youtube>

Implemented motion tracking data pre-fill for compositor nodes
(blender/blender@da852457040,
blender/blender@c76e1ecac6d)

## Clip Editor

- Default to descending average sorting
  (blender/blender@2e6cd704739)

## Mask Editor

- Add mask blending factor for combined overlay
  (blender/blender@eca0c95d51a)
- Add mask spline visibility overlay option
  (blender/blender@df2ab4e758c)
- Masks are always drawn smooth
  (blender/blender@c1ffea157c5)

## Sequencer

- Add filter method to strip transform
  (blender/blender@1c5f2e49b7bf)
- Now change menu (`C` key) supports change scene.
  (blender/blender@205c6d8d0853)
- Add Scene and Strip at the same time in one action.
  (blender/blender@be84fe4ce1d8)
- Delete Strip and Scene in one step.
  (blender/blender@8c4bd02b067a)
- Added new retiming system. Sped up strips can be edited as normal
  strips. Also movie playback speed is adjusted to match scene
  framerate.
  (blender/blender@302b04a5a3fc)
- Add API function to select displayed meta strip.
  (blender/blender@6b35d9e6fbef)

## FFmpeg

- Playback of VFR movies is now correct
  (blender/blender@f0a3d2beb23e)
