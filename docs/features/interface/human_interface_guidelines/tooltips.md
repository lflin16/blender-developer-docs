# Tooltips

!!! Info "Guideline status"
    Needs approval

Tooltips are little popups that give more information about the element
under the mouse cursor.

## General rules

Follow the general [writing style
guidelines](writing_style.md).

What tooltips should contain:

- The name of the item, not followed by a period.
- A description of the item (see
  [below](#the-item-description)).
- A "disabled hint" for disabled buttons: If a button is disabled, this
  text will explain why. (TODO document how to set this!).
- The shortcut, if any.
- The library path, if any.
- Additional <b>useful</b> information (expressions for drivers, BPY
  path, etc. - *These fields may be automatically generated and may be
  disabled through a Preference option*).

## The Item Description

Tooltips should first and foremost be helpful. If more than a short
sentence is needed to achieve that, by all means, use more than one.
Yet, try to keep it short. In other words: Short **and** to the
point.</b>

- It is recommended to start descriptions with verbs. Using the
  [imperative
  conjugation](https://en.wikipedia.org/wiki/Imperative_mood) generally
  works best.  
  ***Don't**: "The factor determine**s** how the geometry end factor is
  mapped to a spline"  
  **Do**: "Determine how the geometry end factor is mapped to a spline"*
- Do **not** use more than a couple of lines, try to stick with two or
  three.
- Use full sentences (periods and commas are allowed).
- It's fine to mention examples of situations where you might use the
  tool.
- Do **not** document default values; these can change and are hard to
  keep updated.
- Do **not** use the described term to explained itself.  
  \* *Limit Surface  
  **Don't:** "Put vertices at the limit surface"  
  **Do**: "Place vertices at the surface that would be produced with
  infinite levels of subdivision (smoothest possible object)".*
  - *Proportional Editing  
    **Don't**: "Enable Proportional Editing"  
    **Do**: "Transform the neighbouring elements in a falloff from the
    selection"*
- Explanations of how the result is affected by different values are
  fine.  
  *"Higher values reduce render time, lower values render with more
  detail."*
- Only describe the *positive* state of a toggle, **not** the negative  
  ***Don't**: "Hide in Viewport"  
  **Do**: "Show in Viewport"*
- Do **not** use redundant words such as "*enables*", "*activates*"
  etc.  
  ***Don't**: "Enables snapping during transform"  
  **Do**: "Snap during transform"*
