# Main Generator Engine

This describes the main classes that manage the generation of a rig.

## Generator

The rig generation process is managed by an instance of the Generator
class. It duplicates the metarig, instantiates the rig objects, creates
the root control bone, executes the generation stages, and manages some
other global operations.

### Fields

The generator instance has the following fields:

`context`  
:   Blender context in which the generation was invoked.

`scene`  
:   Current scene.

`view_layer`  
:   Current view layer.

`layer_collection`  
:   Current layer collection.

`collection`  
:   Current collection.

`metarig`  
:   The original metarig object.

`obj`  
:   The rig object being generated.

`stage`  
:   The name of the current stage being executed.

`script`  
:   The [ScriptGenerator](script_generator.md)
    object that manages generation of the UI script.

`artifacts`  
:   The [ArtifactManager](generator.md#artifactmanager)
    object that manages additional helper Objects produced by rig
    generation.

`rig_id`  
:   The random identifier string for the rig being generated.

`rig_list`  
:   A list of all rig instances.

`root_rigs`  
:   A list of all rig instances without a parent rig.

`bone_owners`  
:   A map from bone names to the rig instance that owns them.

`derived_bones`  
:   A map from bone names to sets of bones created by copying from them.

### Methods

These are the public methods of the Generator class:

`generator.disable_auto_parent(bone_name)`  
:   By default, all bones left without a parent are parented to the root
    control. This method excludes a bone from this function.

`generator.find_derived_bones(bone_name, *, by_owner=False, recursive=True)`  
:   Returns a set of bones copied from the specified one. The `by_owner`
    option limits search to the owner rig of the bone. The `recursive`
    option enables walking through multiple chained copies.

`generator.set_layer_group_priority(bone_name, layers, priority)`  
:   Assign a priority value used for choosing which layer to base the bone
    group choice on for the given bone. The value is set for the given
    layers.

`generator.rename_org_bone(old_name, new_name)`  
:   May be used from `SubstitutionRig.substitute()` to rename ORG bones.

`generator.find_rig_class(rig_type)`  
:   Looks up a rig class by its ID string.

`generator.describe_rig(rig)`  
:   Returns a string containing the rig class and base bone.

## LegacyRig

Rigs that don't inherit from the `BaseRig` class are considered to use
the legacy interface, and are wrapped in a `rigify.base_generate.LegacyRig`
wrapper class. The wrapper itself interfaces.
    
The old style `generate` method is invoked during the
`generate_bones` stage, because that is the only stage that can create
bones.

Fields:

`wrapped_rig`  
:   The instance of the legacy rig wrapped by the class.

## SubstitutionRig

In some special cases (mainly backward compatibility) it is necessary to
present one or more different rig components as one, with the actual set
of rigs depending on the structure and parameters of the rig.

This can be achieved by implementing a `Rig` class that inherits from
`rigify.base_generate.SubstitutionRig`. Such classes are handled
specially during the rig instantiation phase: instead of saving the
newly created rig instance, the Generator immediately invokes its
`substitute()` method, and uses the list of rig instances returned
from it instead.

Fields:

`generator`  
:   The instance of Generator.

`obj`  
:   The rig being generated.

`base_bone`  
:   The name of the main bone that instantiated the rig.

`params`  
:   The parameters attached to the main bone.

`params_copy`  
:   Snapshot of the parameters attached to the main bone.

Methods:

`rig.substitute()` *callback*  
:   Invoked immediately after instantiation and expected to return a list of
    rig instances.

    If it is necessary, this method may temporarily switch to edit mode to
    modify bone parenting and/or orientations.

`rig.get_params(bone_name)`  
:   Returns the parameters container for the given bone.

`rig.assign_params(bone_name, param_dict=None, ...)`  
:   Assigns values into the parameter container of the given bone. Parameter
    values may be supplied as a dictionary, or directly as named parameters.
    If both are supplied, the named parameters have precedence.

`rig.instantiate_rig(rig_class, bone_name)`  
:   Creates a rig instance of the given type (specified as a class, or an ID
    string) for the specified bone.

## GeneratorPlugin

Some functionality within the generation process isn't naturally tied to
any specific rig component, and is instead equally shared between all of
them. The generation of the UI script is one such example.

Such features can be conveniently implemented via classes that inherit
from `rigify.base_generate.GeneratorPlugin`. Due to the metaclass and
base classes used by GeneratorPlugin, such classes become parameterized
singletons, which can have the same stage callbacks as regular rig
classes.

### Fields

`priority` *static*  
:   Stage callbacks of the generator plugin instances are invoked after all
    of the rig callbacks are completed, in the order of decreasing priority.

`generator`  
:   The Generator instance.

`obj`  
:   The rig being generated.

### Methods

`rig.__init__(generator)`  
:   Initializes the plugin instance. As a parameterized singleton, every
    unique set of constructor parameter values creates a separate class
    instance, while passing the same values result in the same object.

## ArtifactManager

This class manages additional objects that are created as a part of
generating the rig and are owned by it. It tracks them across multiple
re-generations of the rig to avoid duplicates, and deletes them when not
needed anymore.

All artifact objects must be uniquely identified by their owner rig
component (its base bone) and an arbitrary name assigned within it. At
the end of generation any previously existing artifacts that were not
touched during this generation pass are deleted.

Artifacts must not be deleted during generation other than through
methods of this class, or it can cause a crash.

### Methods

`artifacts.create_new(owner: BaseRig, obj_type: str, name: str) -> Object`  
:   Creates an artifact object of the specified type (same values as
    `object.type`) and name from scratch. If an artifact with that name
    already exists from a previous generation, all references are updated to
    point to the new instance, and the existing one is deleted.

`artifacts.find_or_create(owner: BaseRig, obj_type: str, name: str, *, recreate=False) -> tuple[bool, Object]`  
:   Creates an artifact object of the specified type and name, or reuses the
    existing one. If there is a type mismatch, the existing object is
    deleted as if it didn't exist.
    
    The `recreate` parameter is used to implement `create_new`, i.e.
    makes the method always create a new instance, replacing the existing
    one if necessary.
    
    Returns a flag signifying if an existing instance was found, and the
    found or created object.

`artifacts.new_temporary(owner: BaseRig, obj_type: str, name="temp") -> Object`  
:   Creates a new temporary artifact of the given type. Temporaries are
    always deleted at the end of generation and are tracked separately from
    the permanent artifacts. The name only matters for diagnostics and
    naming the Object.

`artifacts.remove_temporary(obj: Object)`  
:   Immediately deletes the temporary object that was previously created
    using `new_temporary`.
