# License & Copyright Headers

Source files should have both an SPDX copyright and license header.

## New Files

* For choice of license, generally use the same license as other source
  files in the folder. If not, notify the code reviewer.
  Most code is `GPL-2.0-or-later`, but not all.
* Use `<Current Year> Blender Authors` for copyright text.

Example:
```
SPDX-FileCopyrightText: 2024 Blender Authors

SPDX-License-Identifier: GPL-2.0-or-later 
```

## Editing Files

* If `Blender Authors` copyright is not there, add a new line for it
  with the current year. Do not remove existing copyright text.
* If it is already there and you have made significant changes, you can
  optionally extend the range to include the current year. But you are
  not required to.
  For example `2001-2024 Blender Authors`.

## Adapting Code

When adding code from other projects:

* Check the license and find the matching identifier in
  `doc/license/SPDX-license-identifiers.txt`. If it's not listed
  there, discuss with the code reviewer if the code can be used.
* Preserve the copyright text from the original file in a
  `SPDX-FileCopyrightText` header.
* Add `Blender Authors` copyright if you modified the code.

Example:
```
SPDX-FileCopyrightText: 2009-2010 Sony Pictures Imageworks Inc., et al. All Rights Reserved.
SPDX-FileCopyrightText: 2011-2022 Blender Authors

SPDX-License-Identifier: BSD-3-Clause

Adapted code from Open Shading Language.
```
